var socket = io("https://accrete.caltech.edu:8080");
var heartbeat_ts = + new Date();
var doing_heartbeat = false;

function start_geoloc(){
  /*var output = document.getElementById("location");*/
  if ("geolocation" in navigator) {
    /* geolocation is available */
    function geolocation_success(position){
      var latitude = position.coords.latitude;
      var longitude = position.coords.longitude;
      /*var output = document.getElementById("location");
      output.innerHTML = "Lat "+latitude+", Long "+longitude+".";*/
      app.app_state["latitude"] = latitude;
      app.app_state["longitude"] = longitude;
      if (app.hasOwnProperty("player_marker")){
        app["player_marker"].setLngLat([app.app_state.longitude, app.app_state.latitude]);
      }
      //app.ui.update_text("lat"+latitude+"long"+longitude);
    }
    function geolocation_error(error) {
      alert("Error in HTML5 Geolocation. Code: "+ error.code + " Message: " + error.message);
    }
    navigator.geolocation.getCurrentPosition(geolocation_success, geolocation_error, {enableHighAccuracy: true});
    navigator.geolocation.watchPosition(geolocation_success, geolocation_error, {enableHighAccuracy: true});
    init_map();
  } else {
    /* geolocation IS NOT available */
    alert("HTML5 Geolocation not available.");
  }

  /*loadJSON('map-data.json',
  init_map,
  function(xhr){alert("Error loading JSON. "+xhr)});*/
}

function loadJSON(path, success, error){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function(){
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        if (success) success(JSON.parse(xhr.responseText));
      } else {
        if (error) error(xhr);
      }
    }
  };
  xhr.open("GET", path, true);
  xhr.send();
}

function load_geojson(data){
  //console.log(data);
  //var dataLayer = L.geoJson(data);
  //dataLayer.addTo(app.app_state["map"])
  mod_data = data
  for(var x=0;x< mod_data.features.length;x++){
    if(mod_data.features[x].properties.tags.hasOwnProperty("name")){
      mod_data.features[x].properties["title"] = mod_data.features[x].properties.tags["name"]
    }
  }
  opts_fill = {
    "id": "fill",
    "type": "fill",
    "source": {
      "type": "geojson",
      "data": data
    },
    "layout": {},
    "paint": {
      "fill-color": "#088",
      "fill-opacity": 0.8
    }
  }
  opts_symbols = {
    "id": "points",
    "type": "symbol",
    "source": {
      "type": "geojson",
      "data": mod_data
    },
    "layout": {
        "icon-image": "circle-15",
        "text-field": "{title}",
        "text-font": ["Open Sans Regular", "Arial Unicode MS Bold"],
        "text-offset": [0, 0.6],
        "text-anchor": "top"
    }
  }

  // create a DOM element for the marker
  var el = document.createElement('div');
  var zoom_level = 17;
  pix_size = calc_pix_and_offset(zoom_level)
  el.className = 'marker';
  el.id = 'player-icon';
  //document.getElementById("player-icon").style.backgroundImage = 'url(https://accrete.caltech.edu:8080/img/' + Cookies.get("sprite") + "-front.gif"+')';
  el.style.backgroundImage = 'url(https://accrete.caltech.edu:8080/img/' + Cookies.get("sprite")+"-front.gif"+')';
  el.style.width = pix_size + 'px';
  el.style.height = pix_size + 'px';

  /*el.addEventListener('click', function() {
    window.alert("Clicked on player");
  });*/

  mapboxgl.accessToken = 'pk.eyJ1IjoiaXJseXJhZSIsImEiOiJjajJoNzh5bnowMDduMzJuNWd5bHV3a2FlIn0.uEz7bdUVlY1UBtuXIfrweA';
  app.app_state["map"] = new mapboxgl.Map({
      container: 'map', // container id
      style: 'mapbox://styles/mapbox/streets-v9',
      center: [app.app_state.longitude, app.app_state.latitude], // starting position
      zoom: zoom_level, // starting zoom,
      pitch: 60,
      bearing: 0,
      attributionControl:false
  });

  // Add zoom and rotation controls to the map.
  app.app_state["map"].addControl(new mapboxgl.NavigationControl(), 'top-left');

  // add marker to map
  app["player_marker"] = new mapboxgl.Marker(el, {offset: [-pix_size / 2, -pix_size / 2]});
  app["player_marker"].setLngLat([app.app_state.longitude, app.app_state.latitude])
      .addTo(app.app_state["map"]);
  app.app_state["map"].on("load",function(){
    app.app_state["map"].setMinZoom(14);
    app.app_state["map"].addLayer(opts_symbols);
    app.app_state["map"].addSource("player", {
      "type": "geojson",
      "data": {
          "type": "Point",
          "coordinates": [
              app.app_state.longitude, app.app_state.latitude
          ]
      }
    });
  app.app_state["map"].on('click', 'points', function (e) {
    if (typeof e.features[0].geometry.coordinates[0][0] == "object"){
      avg_lng = e.features[0].geometry.coordinates[0][0][0]
      avg_lat = e.features[0].geometry.coordinates[0][0][1]
      //console.log(e.features[0].geometry.coordinates[0])
      //console.log([avg_lng, avg_lat])
      for (var idx = 1; idx < e.features[0].geometry.coordinates[0].length; idx++){
        avg_lng += e.features[0].geometry.coordinates[0][idx][0]
        avg_lat += e.features[0].geometry.coordinates[0][idx][1]
      }
      avg_lng /= e.features[0].geometry.coordinates[0].length
      avg_lat /= e.features[0].geometry.coordinates[0].length
      //console.log([avg_lng, avg_lat])
      app.app_state["map"].flyTo({center: [avg_lng, avg_lat]});
    }
    else{
      app.app_state["map"].flyTo({center: e.features[0].geometry.coordinates});
    }
  });
  app.app_state["map"].on('rotate', function(e){
    // if from -pi/2 to pi/2, front
    // otherwise back.
    if(Math.random() > 0.5){
      sort_zindex();
    }

    if(Math.abs(app.app_state.map.transform.angle) > Math.PI/2. && app.app_state.front){
      document.getElementById("player-icon").style.backgroundImage = 'url(https://accrete.caltech.edu:8080/img/' + Cookies.get("sprite") + "-back.gif"+')';
      if (app.hasOwnProperty("other_players")){
        for (var player in app["other_players"]){
          document.getElementById("other-player-icon-"+player).style.backgroundImage = 'url(https://accrete.caltech.edu:8080/img/' + app["other_players"][player].sprite + "-back.gif"+')';
        }
      }
      app.app_state.front = false;
    }
    else if(Math.abs(app.app_state.map.transform.angle) < Math.PI/2. && !app.app_state.front){
      document.getElementById("player-icon").style.backgroundImage = 'url(https://accrete.caltech.edu:8080/img/' + Cookies.get("sprite") + "-front.gif"+')';
      if (app.hasOwnProperty("other_players")){
        for (var player in app["other_players"]){
          document.getElementById("other-player-icon-"+player).style.backgroundImage = 'url(https://accrete.caltech.edu:8080/img/' + app["other_players"][player].sprite + "-front.gif"+')';
        }
      }
      app.app_state.front = true;
    }
  });
    console.log('map loaded');
    var button = document.createElement("button");
    button.setAttribute("id", "center_character");
    button.style.backgroundImage = "url('img/"+Cookies.get('sprite')+".png')";
    button.addEventListener("click", function(){
      app.app_state["map"].flyTo({center: [app.app_state.longitude, app.app_state.latitude]});
    })
    document.getElementsByClassName("mapboxgl-ctrl-top-left")[0].children[0].appendChild(button);
    var button_toggle_fullscreen = document.createElement("button");
    button_toggle_fullscreen.setAttribute("id", "toggle_fullscreen");
    button_toggle_fullscreen.style.backgroundImage = "url('img/fullscreen-24.png')";
    button_toggle_fullscreen.addEventListener("click", function(){
      screenfull.toggle();
    })
    document.getElementsByClassName("mapboxgl-ctrl-top-left")[0].children[0].appendChild(button_toggle_fullscreen);
    //
    var right_ctrl_group = document.createElement("div")
    right_ctrl_group.setAttribute("class","mapboxgl-ctrl mapboxgl-ctrl-group");
    document.getElementsByClassName("mapboxgl-ctrl-top-right")[0].appendChild(right_ctrl_group);
    //
    var quests = document.createElement("button");
    quests.setAttribute("id", "quests");
    //quests.style.backgroundImage = "url('img/fullscreen-24.png')";
    /*var book = document.createElement("button");
    book.setAttribute("id", "book");
    book.style.backgroundImage = "url('img/spellbook.png')";
    book.addEventListener("click", function(){
      var overlay = document.getElementById("overlay");
      var close_overlay = document.createElement("button");
      close_overlay.setAttribute("id", "close-overlay");
      close_overlay.addEventListener("click",function(){
        overlay.style.display = "none";
        overlay.innerHTML = "";
      }, {"once": true});
      overlay.appendChild(close_overlay);
      var book_binding = document.createElement("div");
      book_binding.setAttribute("id", "wrapper");
      var book_page = document.createElement("div");
      book_page.setAttribute("id", "slider");
      book_page.setAttribute("class", "swipe");
      var book_page_wrap = document.createElement("div");
      function render_page_from_book_page(page_arr){
        var book_page = document.createElement("div");
        var img = document.createElement("img");
        img.src = page_arr[0];
        var p1 = document.createElement("p");
        p1.setAttribute("class", "suptext");
        p1.innerHTML=page_arr[1];
        var p2 = document.createElement("p");
        p2.setAttribute("class", "text");
        p2.style.fontStyle = "italic";
        p2.innerHTML=page_arr[2];
        var p3 = document.createElement("p");
        p3.setAttribute("class", "text");
        p3.style.fontStyle = "italic";
        p3.innerHTML=page_arr[3];
        book_page.appendChild(img);
        book_page.appendChild(p1);
        book_page.appendChild(p2);
        book_page.appendChild(p3);
        return book_page;
      }
      var book_pages = [["img/birdcage.jpg","BIRD CAGE","","As a young bird, you are about to leave the cage of your own preconceptions and exit into a shiny new world. Spread your wings, fly free, and you will surely find your nest."],
       ["img/notebook.jpg","GHOST NOTEBOOK", "Noctu Orfei", ""],
       ["img/whistle.jpg","TURTLE WHISTLE", "Aude Fraetor", "Isn’t it cute?"],
       ["img/goldenheart.jpg","GOLDEN HEART OF A MONSTER", "Phaidoari Afairynghor", "When you look into the eyes of the enemy, you see a monster. But when the enemy looks into your eyes, they too see a monster. Yet what you have found is that despite your animosity, it too had a heart of gold all along. Your magic is not merely a tool to combat your enemies—it is a tool to bring out the light in them as well."],
       ["img/lightbulb.jpg","LIGHT BULB", "Arae Aryrha", "The power of the shiny rod reveals itself only when Akko’s own inner light shines through. Like Akko herself, the lightbulb brings light to the world and allows others to see how they can use their own strengths."],
       ["img/staghead.jpg","STATUE OF THE HEAD OF THE STAG KING OF THE FOREST", "Mayenab Dysheebudo", 'A hunter came upon the overgrown shrine to the stag king of the forest. An apparition of the stag appeared to him and intoned, “Look upon my works, ye mighty, and despair.” What the stag king had in life now lies in ruins. The hunter nodded, scratched at his scraggly beard, and hacked the head of the stag king from its body. “This’ll fetch a good price,” he said. “It even talks!”'],
       ["img/chariot.jpg","TAROT CARD WITH SHINY CHARIOT", "Sybilladura Lelladybura", "After seeing one of Shiny Chariot’s famous illusion shows, Akko Kagari was instantly mesmerized, fascinated, obsessed. When her parents took her past a department store window, something caught her eye: an image of Chariot, collectible, laminated and full of the promise of magic. She knew she had to buy it and soon she was buying as many as she could. This one, though, is her favorite and she keeps it on her at all times, both as a kind of talisman and an ever-present reminder of what she aspires to be."]];
      var empty_page = ["nothing.jpg", "", "", "You sense faint magical humming, but the words are illegible."];
      var current_book_pages = book_pages.slice(0, app.state_num).concat(Array(7 - app.state_num).fill(empty_page));
      console.log(current_book_pages);
      for (var bidx = 0; bidx < current_book_pages.length; bidx++){
        book_page_wrap.appendChild(render_page_from_book_page(current_book_pages[bidx]));
      }
      book_page_wrap.setAttribute("class", "swipe-wrap");
      book_page.appendChild(book_page_wrap);
      book_binding.appendChild(book_page);
      overlay.appendChild(book_binding);
      overlay.style.display = "inline-block";
      overlay.style.backgroundColor = "white";
      overlay.style.backgroundImage = "url('https://accrete.caltech.edu:8080/img/luna_nova.png')";
      overlay.style.overflow = "scroll";
      window.mySwipe = Swipe(document.getElementById('slider'));
    });*/
    //book.style.backgroundImage = "url('img/fullscreen-24.png')";
    /*var moonrunes = document.createElement("button");
    moonrunes.setAttribute("id", "moonrunes");
    moonrunes.style.backgroundImage = "url('img/moonrune_icon.png')";
    moonrunes.addEventListener("click", function(){
      var overlay = document.getElementById("overlay");
      var close_overlay = document.createElement("button");
      close_overlay.setAttribute("id", "close-overlay");
      close_overlay.addEventListener("click",function(){
        overlay.style.display = "none";
        overlay.innerHTML = "";
      }, {"once": true});
      overlay.appendChild(close_overlay);
      var moonrunes_page = document.createElement("img");
      moonrunes_page.src = "img/moonrunes.png";
      overlay.appendChild(moonrunes_page);
      overlay.style.display = "inline-block";
      overlay.style.backgroundColor = "white";
      overlay.style.backgroundImage = "url('https://accrete.caltech.edu:8080/img/luna_nova.png')";
      overlay.style.overflow = "scroll";
    });*/
    //document.getElementsByClassName("mapboxgl-ctrl-top-right")[0].children[0].appendChild(quests);
    //document.getElementsByClassName("mapboxgl-ctrl-top-right")[0].children[0].appendChild(book);
    //document.getElementsByClassName("mapboxgl-ctrl-top-right")[0].children[0].appendChild(moonrunes);
    init_loc_marker();
  });

  /*app.app_state["map"].on('moveend', function(){
    resize_some_icon('player-icon');
  });*/
}

function init_map(){
  var loc = location.href;
  if(app.web){
    var caltech_geojson_url = loc+"js/map-data.json";
    var arboretum_geojson_url = loc+"js/arboretum.json";
  }
  else{
    var caltech_geojson_url = loc.substring(0,loc.length-11)+'/js/map-data.json';
    var arboretum_geojson_url = loc.substring(0,loc.length-11)+"js/arboretum.json";
  }
  /*loadJSON(caltech_geojson_url, load_geojson, function(error){
    alert("Error in obtaining GeoJSON resources: " + error);
  });*/
  loadJSON(arboretum_geojson_url, load_geojson, function(error){
    alert("Error in obtaining GeoJSON resources: " + error);
  });
  //document.getElementById("map").setAttribute("style","width:"+window.innerWidth+"px;height:"+window.innerHeight+"px");
}

function init_ui(){
  app["ui"] = {
    "element": document.getElementById("ui"),
    "text_queue": [],
    "update_text": function(text){
      app.ui.text_queue.push(text);
      if(app.ui.element.style.display == "none"){
        //console.log(app.ui.element.style.display == "none");
        //console.log(app.ui.element.style.display);
        app.ui.next_text();
      }
    },
    "update_text_block": function(text_arr){
      for(var idx=0;idx<text_arr.length;idx++){app.ui.text_queue.push(text_arr[idx])}
      //console.log(app.ui.element.style.display == "none");
      //console.log(app.ui.element.style.display);
      if(app.ui.element.style.display == "none"){
        app.ui.next_text();
      }
    },
    "insert_next_text": function(text_arr){
      app.ui.text_queue.unshift(text_arr);
      //console.log(app.ui.element.style.display == "none");
      //console.log(app.ui.element.style.display);
      if(app.ui.element.style.display == "none"){
        app.ui.next_text();
      }
    },
    "next_text": function(){
      //console.log(app.ui.text_queue[0]);
      var width = window.innerWidth;
      app.ui.element.style.display = "block";
      app.ui.element.innerHTML = "";
      if (app.ui.text_queue.length > 0){
        if (app.ui.text_queue[0].hasOwnProperty("suptext")){
          var sup_text = document.createElement("p");
          sup_text.setAttribute("class", "suptext");
          var portrait_text = "";
          if (app.ui.text_queue[0].hasOwnProperty("image")){
            portrait_text = "<img src='"+app.ui.text_queue[0].image+"' class='portrait' />";
          }
          var sup_text_stuff = app.ui.text_queue[0]["suptext"];
          sup_text.innerHTML = portrait_text + sup_text_stuff;
          app.ui.element.append(sup_text);
        }
        var normal_text = document.createElement("p");
        normal_text.innerHTML = app.ui.text_queue[0].text;
        normal_text.setAttribute("class", "text");
        app.ui.element.append(normal_text);
        if (app.ui.text_queue[0].hasOwnProperty("alternative")) {
          alternative_val = app.ui.text_queue[0].alternative();
          if(alternative_val[0]){
            // then skip.
            app.ui.text_queue.shift();
            app.ui.text_queue.unshift(alternative_val[1]);
            app.ui.next_text();
            return true;
          }
        }
        if (app.ui.text_queue[0].hasOwnProperty("has_question") && app.ui.text_queue[0].has_question && app.ui.text_queue[0].hasOwnProperty("id")){
          //it has a question!
          var form_input = document.createElement("input");
          form_input.setAttribute("id", "form_input");
          form_input.setAttribute("type", "text");
          form_input.setAttribute("data-id", app.ui.text_queue[0].id);
          app.ui.element.appendChild(form_input);
          app.ui.element.onclick = function(){};
          document.getElementById("form_input").addEventListener("focus", function(){
            app.ui.element.style.top = "0px";
            app.ui.element.style.bottom = "";
          });
          document.getElementById("form_input").addEventListener("blur", function(){
            app.ui.element.style.top = "";
            app.ui.element.style.bottom = "0px";
          });
          document.getElementById("form_input").addEventListener("keydown", function(e){
            var code = e.keyCode ? e.keyCode : e.which;
            if (code === 13) {
              socket.emit(document.getElementById("form_input").getAttribute("data-id"), document.getElementById("form_input").value);
              document.getElementById("form_input").blur();
              app.ui.element.style.top = "";
              app.ui.element.style.bottom = "0px";
            }
          });
        }
        app.ui.text_queue.shift();
        //app.ui.element.style = "animation:fadein 2s;display:block;width:"+width+"px;bottom: 0px;left: 50%;transform: translate(-50%,0%);"
      }
      else {
        app.ui.element.innerHTML = "";
        app.ui.element.style.display = "none";
        //app.ui.element.style = "display:none;width:"+width+"px;bottom: 0px;left: 50%;transform: translate(-50%,0%);"
      }
    }
  }
  app.ui.element.onclick = app.ui.next_text;
  document.addEventListener("DOMContentLoaded",function(){
    app.ui.element.style.display = "block";
    app.ui.element.style.width = window.innerWidth+"px";
    app.ui.element.style.bottom = "0px";
    app.ui.element.style.left = "50%";
    app.ui.element.style.transform  = "translate(-50%,0%)";
    app.ui.element.style.display = "none";
    app.ui.update_text_block([{
      "alternative": function(){
        var value = typeof Cookies.get("name") == "string" && Cookies.get("name").length > 0;
        if(value){
          socket.emit("name", Cookies.get("name"));
        }
        return [value,{"text":"Logging you in, "+Cookies.get("name")}];
      },
      "suptext":"Announcer Duck",
      "text":"Please enter your name! (Must be unique, so you can use nicknames!)",
      "image":"img/duck.gif",
      "has_question": true,
      "id": "name"
    }]);
    app["team"] = {
      "red": {"element": document.createElement("div")},
      "blue": {"element": document.createElement("div")}}
    app.team.red.element.style.position = "fixed";
    app.team.red.element.style.display = "block";
    app.team.red.element.style.height = "80px";
    app.team.red.element.style.width = "90px";
    app.team.red.element.style.backgroundColor = "#eee";
    app.team.red.element.style.borderWidth = "1px";
    app.team.red.element.style.borderStyle = "solid";
    app.team.red.element.style.bottom = "152px";
    app.team.red.element.style.left = "0px";
    app.team.red.element.style.zIndex = 48;
    app.team.red.element.style.background = "url('img/team_akko.png') no-repeat center";
    app.team.red.element.style.backgroundPosition = "50% 100%";
    app.team.red.element.style.textAlign = "center";
    app.team.red.element.style.color = "#8b0000";
    app.team.red.element.style.fontSize = "x-large";
    document.body.appendChild(app.team.red.element);
    app.team.red.element.style.display = "none";
    app.team.blue.element.style.position = "fixed";
    app.team.blue.element.style.display = "block";
    app.team.blue.element.style.height = "80px";
    app.team.blue.element.style.width = "90px";
    app.team.blue.element.style.backgroundColor = "#eee";
    app.team.blue.element.style.borderWidth = "1px";
    app.team.blue.element.style.borderStyle = "solid";
    app.team.blue.element.style.bottom = "152px";
    app.team.blue.element.style.right = "0px";
    app.team.blue.element.style.zIndex = 48;
    app.team.blue.element.style.background = "url('img/team_amanda.png') no-repeat center";
    app.team.blue.element.style.backgroundPosition = "50% 100%";
    app.team.blue.element.style.textAlign = "center";
    app.team.blue.element.style.color = "#000080";
    app.team.blue.element.style.fontSize = "x-large";
    document.body.appendChild(app.team.blue.element);
    app.team.blue.element.style.display = "none";
  });
}

//window.onorientationchange = function

function calc_pix_and_offset(desired_zoom_level){
  /*ref_pix_size = 20;
  zoom_level = 15;
  pix_size = ref_pix_size*Math.pow(2.,desired_zoom_level-zoom_level);*/
  pix_size = 100;
  return pix_size
}

/*function resize_some_icon(id_name){
  pix_size = calc_pix_and_offset(app.app_state["map"].getZoom())
  app.player_marker._offset.x = -pix_size/2
  app.player_marker._offset.y = -pix_size/2
  el = document.getElementById(id_name);
  el.style.width = pix_size + 'px';
  el.style.height = pix_size + 'px';
  app.app_state["map"]._render();
}*/

/*document.onkeydown = function (event) {
  // Auto-focus the current input when a key is typed
  if (!(event.ctrlKey || event.metaKey || event.altKey)) {
    document.getElementById("form_input").focus();
  }
  // When the client hits ENTER on their keyboard

}*/

socket.on("state", function(state_num){
  if (state_num > 1 && app.state_num < 2){
    if(Math.abs(Math.abs(state_num - app.state_num) - 1.) > 0.1){
      for(var idx=1; idx < state_num; idx++){
        for(var nidx=0; nidx < quests[quest_order[idx]].length; nidx++){
          if(quests[quest_order[idx]][nidx].hasOwnProperty("alternative")){
            app.app_state["map"].on("load",quests[quest_order[idx]][nidx].alternative);
          }
        }
      }
    }
  }
  app.state = quest_order[state_num];
  app.state_num = state_num;
  app.ui.update_text_block(quests[app.state]);
});

socket.on("name_success", function (data){
  //we immediately set name to our own Cookies.
  //we have a personal name too.
  var previously_set = Cookies.get("name");
  Cookies.set("name", data["name"]);
  Cookies.set("sprite", data["sprite"]);
  Cookies.set("team", data["team"]);
  setInterval(function(){
    socket.emit("gps_update",{"name": Cookies.get("name"),
    "coords": [app.app_state.longitude, app.app_state.latitude]})
  }, 5000);
  app.ui.element.onclick = app.ui.next_text;
  app.ui.next_text();
  if(typeof previously_set == "undefined"){
    setTimeout(function(){play_video_callback("media/lwa-title.mp4", function(){
      document.getElementById("player-icon").style.backgroundImage = 'url(https://accrete.caltech.edu:8080/img/' + Cookies.get("sprite") + "-front.gif"+')';
      document.getElementById("center_character").style.backgroundImage = "url('img/"+Cookies.get('sprite')+".png')";
    })},300);
  }
  else{
    setTimeout(function(){app.app_state["map"].jumpTo({center: [app.app_state.longitude, app.app_state.latitude], zoom: zoom_level});}, 300);
  }
  socket.emit("need_state");
});

function sort_zindex(){
  var z_indices_sprites = [4,5,6,7,8,9];
  var sorting_zindex = [];
  var temp_y = k =document.getElementById("player-icon").style.transform.split(",")[1].slice(1,document.getElementById("player-icon").style.transform.split(",")[1].length-3);
  temp_y = +temp_y
  sorting_zindex.push(["player-icon", temp_y])
  if (app.hasOwnProperty("other_players")){
    for (var player in app["other_players"]){
      var temp_y = k =document.getElementById("other-player-icon-"+player).style.transform.split(",")[1].slice(1,document.getElementById("other-player-icon-"+player).style.transform.split(",")[1].length-3);
      temp_y = +temp_y
      sorting_zindex.push(["other-player-icon-"+player, temp_y])
    }
  }
  sorting_zindex = sorting_zindex.sort(function(a,b){
    if (a[1] < b[1]) return -1;
    if (a[1] > b[1]) return 1;
    return 0;
  });
  //console.log(sorting_zindex)
  for (var idx=0; idx < sorting_zindex.length; idx++){
    //console.log(sorting_zindex[idx][0]);
    document.getElementById(sorting_zindex[idx][0]).style.zIndex = ""+z_indices_sprites[idx];
  }
}

function render_players(all_players){
  if (app.app_state.hasOwnProperty("map")){
    if (!app.hasOwnProperty("other_player_markers")) {
      app["other_player_markers"] = {}
    }
    //console.log("all_players", all_players);
    for (var player in all_players){
      //.log("player", player)
      if (player != Cookies.get("name")){
        if (!app.other_player_markers.hasOwnProperty(player)){
          var el = document.createElement('div');
          pix_size = calc_pix_and_offset(app.app_state["map"].getZoom());
          el.className = 'marker';
          el.id = 'other-player-icon-'+player;
          el.style.backgroundImage = 'url(https://accrete.caltech.edu:8080/img/' + all_players[player].sprite + "-front.gif"+')';
          el.style.width = pix_size + 'px';
          el.style.height = pix_size + 'px';
          app["other_player_markers"][player] = new mapboxgl.Marker(el, {offset: [-pix_size / 2, -pix_size / 2]});
          /*app.app_state["map"].on('moveend', function(){
            resize_some_icon('other-player-icon-'+player);
          });*/
          //console.log('other-player-icon-'+player);
          app["other_player_markers"][player].setLngLat(all_players[player].coords);
          //console.log("hello", app["other_player_markers"][player]);
          app["other_player_markers"][player].addTo(app.app_state["map"]);
        }
        else {
          app["other_player_markers"][player].setLngLat(all_players[player].coords);
        }
      }
    }
    sort_zindex();
  }
}

socket.on("hide_loc_marker", hide_loc_marker);
socket.on("move_loc_marker", move_loc_marker);

socket.on("gps", function(data){
  //console.log(data);
  if (app.app_state.hasOwnProperty("map") && typeof Cookies.get("name") != "undefined"){
    for (var idx=0; idx<data.length; idx++){
      //console.log(data[idx]);
      //console.log(data[idx].name);
      //console.log(data[idx].name, data[idx].coords)
      if (data[idx].name != Cookies.get("name")){
        if (!app.hasOwnProperty("other_players")){
          app["other_players"] = {}
        }
        app.other_players[data[idx].name] = {
          "team": data[idx].team,
          "sprite": data[idx].sprite,
          "coords": data[idx].coords
        }
      }
    }
    //console.log(app.other_players)
    render_players(app.other_players);
  }
  var connection_div = document.getElementById("connection");
  connection_div.innerHTML = "connected";
  connection_div.color = "#008000";
  heartbeat_ts = + new Date();
  if (!doing_heartbeat){
    setInterval(function(){
      var new_ts = + new Date();
      if (new_ts > heartbeat_ts + 7000.){
        connection_div.innerHTML = "disconnected for "+Math.round((new_ts-heartbeat_ts)/1000.)+" seconds. trying to reconnect...";
        connection_div.color = "#8b0000";
      }
    }, 7000);
    doing_heartbeat = true;
  }
});

socket.on("notice", function(data){
  if (data.hasOwnProperty("refresh")){
    if (data.refresh){
      window.location.reload();
    }
  }
  app.ui.insert_next_text(data);
});

function update_all_values(answers){
  for(var prop in answers){
    if(document.getElementById(prop) != null){
      console.log(prop, answers[prop]);
      document.getElementById(prop).value = answers[prop];
      document.getElementById(prop).text = answers[prop];
      document.getElementById(prop).innerHTML = answers[prop];
    }
  }
}

socket.on("points", function (data) {
  //console.log("data",data)
  //the number of points for either team has changed, update point totals.
  app.team.red.element.innerHTML = ""+data.red.points;
  app.team.blue.element.innerHTML = ""+data.blue.points;
  update_all_values(data[Cookies.get("team")].answers);
});

function init_loc_marker(){
  var el = document.createElement('div');
  el.className = 'marker';
  el.id = 'loc_marker';
  //document.getElementById("player-icon").style.backgroundImage = 'url(https://accrete.caltech.edu:8080/img/' + Cookies.get("sprite") + "-front.gif"+')';
  el.style.backgroundImage = 'none';
  el.style.width = '64px';
  el.style.height = '64px';
  app["loc_marker"] = new mapboxgl.Marker(el, {offset: [-64 / 2, -64 / 2]});
  app["loc_marker"].setLngLat([0.,0.]).addTo(app.app_state["map"]);
}

function move_loc_marker(coords){
  var el = document.getElementById("loc_marker");
  el.style.backgroundImage = 'url(https://accrete.caltech.edu:8080/img/star.gif)';
  app["loc_marker"].setLngLat(coords);
}

function hide_loc_marker(){
  el.style.backgroundImage = "none";
  app["loc_marker"].setLngLat([0.,0.]);
}
