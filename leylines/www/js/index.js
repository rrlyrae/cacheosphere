/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var zoom_level = 17;
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        init_ui();
        start_geoloc();
    },
    app_state: {"latitude":0., "longitude":0., "map": null, "front": true},
    web: true,
    state: "breakfast",
    state_num: 0
};

/*function toggleFullScreen() {
  if (!document.fullscreenElement) {
      document.getElementById("app").requestFullscreen();
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    }
  }
}*/
/*function toggleFullScreen() {
  var doc = window.document;
  var docEl = doc.documentElement;

  var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
  var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

  if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
    requestFullScreen.call(docEl);
  }
  else {
    cancelFullScreen.call(doc);
  }
}*/

document.addEventListener("click", function() {
  screenfull.request();
},{"once": true});

function addSourceToVideo(element, src){//,(type) {
    var source = document.createElement('source');

    source.src = src;
    //source.type = type;

    element.appendChild(source);
}

function play_video(url){
  var v = document.getElementById("video-player");
  var vo = document.getElementById("overlay");
  var c = document.getElementById("connection");
  v.style.display = "block";
  vo.style.display = "block";
  vo.style.backgroundColor = "black";
  vo.style.backgroundImage = "none";
  vo.style.overflow = "hidden";
  v.style.height = window.innerHeight-200.+"px";
  v.style.width = v.offsetWidth+"px";
  v.style.left = window.innerWidth/2. - v.offsetWidth/2.+"px";
  v.style.top = window.innerHeight/2. - v.offsetHeight/2.+"px";
  c.style.display = "none";
  var source = document.createElement('source');
  source.src = url;
  source.type = "video/mp4";
  //source.style.display = "block";
  v.appendChild(source);
  v.load();
  v.addEventListener("canplaythrough", function(){
    v.play();
  });
  v.addEventListener("ended", function(){
    v.style.display = "none";
    vo.style.display = "none";
    v.removeChild(source);
    c.style.display = "block";
    app.app_state["map"].jumpTo({center: [app.app_state.longitude, app.app_state.latitude], zoom: zoom_level});
  });
}

function play_video_callback(url, callback){
  var v = document.getElementById("video-player");
  var vo = document.getElementById("overlay");
  var c = document.getElementById("connection");
  v.style.display = "block";
  vo.style.display = "block";
  vo.style.backgroundColor = "black";
  vo.style.backgroundImage = "none";
  vo.style.overflow = "hidden";
  v.style.height = window.innerHeight-200.+"px";
  v.style.width = v.offsetWidth+"px";
  v.style.left = window.innerWidth/2. - v.offsetWidth/2.+"px";
  v.style.top = window.innerHeight/2. - v.offsetHeight/2.+"px";
  c.style.display = "none";
  var source = document.createElement('source');
  source.src = url;
  source.type = "video/mp4";
  //source.style.display = "block";
  v.appendChild(source);
  v.load();
  v.addEventListener("canplaythrough", function(){
    v.play();
  });
  v.addEventListener("ended", function(){
    v.style.display = "none";
    vo.style.display = "none";
    v.removeChild(source);
    c.style.display = "block";
    app.app_state["map"].jumpTo({center: [app.app_state.longitude, app.app_state.latitude], zoom: zoom_level});
    callback();
  });
}

//play_video("media/lwa-archives.mp4")

//app.initialize();
init_ui();
start_geoloc();
