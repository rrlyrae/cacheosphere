var socket = io("https://accrete.caltech.edu:8080");
doing_heartbeat = false;
var rec_idx = -1;

socket.on("gps", function(data){
  var connection_div = document.getElementById("connection");
  connection_div.innerHTML = "connected";
  connection_div.color = "#008000";
  heartbeat_ts = + new Date();
  if (!doing_heartbeat){
    setInterval(function(){
      var new_ts = + new Date();
      if (new_ts > heartbeat_ts + 7000.){
        connection_div.innerHTML = "disconnected for "+(new_ts-heartbeat_ts)/1000.+" seconds. trying to reconnect...";
        connection_div.color = "#8b0000";
      }
    }, 7000);
    doing_heartbeat = true;
  }
});
document.addEventListener("DOMContentLoaded",function(){
  document.getElementById("ursula").addEventListener("keydown", function (event) {
    // When the client hits ENTER on their keyboard
    if (event.which === 13) {
      socket.emit('admin_msg', document.getElementById("ursula").value);
      document.getElementById("ursula").blur();
    }
  });
  document.getElementById("record").addEventListener("click", iterate_record);
  document.getElementById("diana_run").addEventListener("click", function(){
    socket.emit('diana_run', Number(document.getElementById("diana_speed").value));
  });
  document.getElementById("advance_state").addEventListener("click", function(){
    socket.emit('advance_state');
  });
});

function start_geoloc(){
  /*var output = document.getElementById("location");*/
  if ("geolocation" in navigator) {
    /* geolocation is available */
    function geolocation_success(position){
      var emit_obj = {"latitude": position.coords.latitude,
      "longitude": position.coords.longitude,
      "timestamp": + new Date(),
      "rec_idx": rec_idx}
      console.log(emit_obj);
      console.log(rec_idx);
      if (rec_idx > -0.5 && rec_idx < 8){
        socket.emit("admin_record", emit_obj);
      }
      if (rec_idx >7.9){
        socket.emit("done_admin_record",{});
      }
    }
    function geolocation_error(error) {
      alert("Error in HTML5 Geolocation. Code: "+ error.code + " Message: " + error.message);
    }
    navigator.geolocation.getCurrentPosition(geolocation_success, geolocation_error, {enableHighAccuracy: true});
    navigator.geolocation.watchPosition(geolocation_success, geolocation_error, {enableHighAccuracy: true});
  } else {
    /* geolocation IS NOT available */
    alert("HTML5 Geolocation not available.");
  }
}

var path = ["Athenaeum", "Gates Library", "Moore 80", "Guggenheim/Thomas Grassy Area","BBB Lobby", "Church 202", "Firestone (rappeling)", "Tom Mannion's house", "Dabney", "You're done!! Thanks!"]
function iterate_record(){
  start_geoloc();
  rec_idx+=1;
  var rec_elem = document.getElementById("record");
  rec_elem.innerHTML = path[rec_idx];
}

/*[[[-118.05349,34.14642], "West Australia"],
[[-118.05185,34.14607], "East Australia"],
[[-118.05126,34.14580], "Tropical Greenhouse"],
[[-118.05126,34.14453], "Celebration Garden"],
[[-118.05161,34.14415], "Library"],
[[-118.05205,34.14331], "Bauer Lawn and Fountains"],
[[-118.05343,34.14260], "Prehistoric Forest"],
[[-118.05446,34.14012], "Rose Garden"],
[[-118.05521,34.14056], "Coach Barn"],
[[-118.05543,34.14084], "Herb Garden"],
[[-118.05591,34.14112], "Daylily and Magnolia Collections"],
[[-118.05676,34.14067], "Meyberg Waterfall"],
[[-118.05701,34.14015], "Tarlac Knoll"]]*/
