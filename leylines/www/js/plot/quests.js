var quest_order = ["breakfast", "moonrunes", "rhythm", "stringpuzzle", "tunnels", "chemistry", "rappelling", "lunch", "arboretum", "norton", "desert"];

var quests = {
  "breakfast":[
    {
      "text":"Luna Nova has its fair share of mysteries! While wandering around, you lose track of time..."
    },
    {
      "suptext":"Ursula",
      "text":"Breakfast is the most important meal of the day for young witches like yourselves!",
      "image":"img/ursula.png"
    },
    {
      "suptext":"Diana",
      "image":"img/diana.png",
      "text":"Look at the time... To think you would forget such an important breakfast with our wonderful professors..!"
    },
    {
      "suptext":"Diana",
      "image":"img/diana.png",
      "text":"I'm going on ahead. You can follow if you're so inclined.."
    }
  ],
  "moonrunes":[
    {
      "suptext":"Ursula",
      "text":"What is this? Did you find it at The Archives? Could it be a mystical totem?",
      "image":"img/ursula.png"
    },
    {
      "alternative":function(){
        //play_video("media/lwa-moonrunes.mp4");
        // enable totem
        var book = document.createElement("button");
        book.setAttribute("id", "book");
        book.style.backgroundImage = "url('img/spellbook.png')";
        book.addEventListener("click", function(){
          var overlay = document.getElementById("overlay");
          var close_overlay = document.createElement("button");
          close_overlay.setAttribute("id", "close-overlay");
          close_overlay.addEventListener("click",function(){
            overlay.style.display = "none";
            overlay.innerHTML = "";
          }, {"once": true});
          overlay.appendChild(close_overlay);
          var book_binding = document.createElement("div");
          book_binding.setAttribute("id", "wrapper");
          var book_page = document.createElement("div");
          book_page.setAttribute("id", "slider");
          book_page.setAttribute("class", "swipe");
          var book_page_wrap = document.createElement("div");
          function render_page_from_book_page(page_arr){
            var book_page = document.createElement("div");
            var img = document.createElement("img");
            img.style.width = (window.innerWidth - 30)+"px";
            img.style.height = "auto";
            img.style.padding = "10px";
            img.src = page_arr[0];
            var p1 = document.createElement("p");
            p1.setAttribute("class", "suptext");
            p1.style.display = "inline-block";
            p1.innerHTML=page_arr[1];
            var p2 = document.createElement("p");
            p2.setAttribute("class", "text");
            p2.style.textAlign = "center";
            p2.style.fontStyle = "italic";
            p2.innerHTML=page_arr[2];
            var p3 = document.createElement("p");
            p3.setAttribute("class", "text");
            p3.style.fontStyle = "italic";
            p3.innerHTML=page_arr[3];
            book_page.appendChild(img);
            book_page.appendChild(p1);
            book_page.appendChild(p2);
            book_page.appendChild(p3);
            return book_page;
          }
          var book_pages = [["img/birdcage.jpg","BIRD CAGE","","As a young bird, you are about to leave the cage of your own preconceptions and exit into a shiny new world. Spread your wings, fly free, and you will surely find your nest."],
           ["img/notebook.jpg","GHOST NOTEBOOK", "Noctu Orfei", ""],
           ["img/whistle.jpg","TURTLE WHISTLE", "Aude Fraetor", "Isn’t it cute?"],
           ["img/goldenheart.jpg","GOLDEN HEART OF A MONSTER", "Phaidoari Afairynghor", "When you look into the eyes of the enemy, you see a monster. But when the enemy looks into your eyes, they too see a monster. Yet what you have found is that despite your animosity, it too had a heart of gold all along. Your magic is not merely a tool to combat your enemies—it is a tool to bring out the light in them as well."],
           ["img/lightbulb.jpg","LIGHT BULB", "Arae Aryrha", "The power of the shiny rod reveals itself only when Akko’s own inner light shines through. Like Akko herself, the lightbulb brings light to the world and allows others to see how they can use their own strengths."],
           ["img/staghead.jpg","STATUE OF THE HEAD OF THE STAG KING OF THE FOREST", "Mayenab Dysheebudo", 'A hunter came upon the overgrown shrine to the stag king of the forest. An apparition of the stag appeared to him and intoned, “Look upon my works, ye mighty, and despair.” What the stag king had in life now lies in ruins. The hunter nodded, scratched at his scraggly beard, and hacked the head of the stag king from its body. “This’ll fetch a good price,” he said. “It even talks!”'],
           ["img/chariot.jpg","TAROT CARD WITH SHINY CHARIOT", "Sybilladura Lelladybura", "After seeing one of Shiny Chariot’s famous illusion shows, Akko Kagari was instantly mesmerized, fascinated, obsessed. When her parents took her past a department store window, something caught her eye: an image of Chariot, collectible, laminated and full of the promise of magic. She knew she had to buy it and soon she was buying as many as she could. This one, though, is her favorite and she keeps it on her at all times, both as a kind of talisman and an ever-present reminder of what she aspires to be."]];
          var empty_page = ["nothing.jpg", "", "", "You sense faint magical humming, but the words are illegible."];
          var state_num_val = app.state_num;
          if(state_num_val > 7){
            state_num_val = 7;
          }
          var current_book_pages = book_pages.slice(0, state_num_val).concat(Array(7 - state_num_val).fill(empty_page));
          //console.log(current_book_pages);
          for (var bidx = 0; bidx < current_book_pages.length; bidx++){
            book_page_wrap.appendChild(render_page_from_book_page(current_book_pages[bidx]));
          }
          book_page_wrap.setAttribute("class", "swipe-wrap");
          book_page.appendChild(book_page_wrap);
          book_binding.appendChild(book_page);
          overlay.appendChild(book_binding);
          overlay.style.display = "inline-block";
          overlay.style.backgroundColor = "white";
          overlay.style.backgroundImage = "url('https://accrete.caltech.edu:8080/img/luna_nova.png')";
          overlay.style.overflow = "scroll";
          window.mySwipe = Swipe(document.getElementById('slider'));
        });
        document.getElementsByClassName("mapboxgl-ctrl-top-right")[0].children[0].appendChild(book);
        return [false,{}];
      },
      "suptext":"Announcer Duck",
      "text":"Congratulations, you have unlocked the 'totem' spellbook!",
      "image":"img/duck.gif"
    },
    {
      "suptext":"Announcer Duck",
      "text":"Congratulations, you have unlocked the 'bird cage' totem!",
      "image":"img/duck.gif"
    },
    {
      "text":"Something tells you that the spellbook will come in handy later."
    },
    {
      "text":"It's time for class, and, well, it's magical linguistics again..."
    },
    {
      "alternative": function(){
        //play_video("media/lwa-moonrunes.mp4");
        return [false, {}];
      },
      "suptext":"Diana",
      "image":"img/diana.png",
      "text":"I sure hope you studied up on your moonrunes..."
    }
  ],
  "rhythm":[
    {
      "text":"You feel like you learned a lot! The professor even gave you a cheatsheet for your upcoming test!"
    },
    {
      "alternative":function(){
        //play_video("media/lwa-intro.mp4");
        // enable moonrunes
        var moonrunes = document.createElement("button");
        moonrunes.setAttribute("id", "moonrunes");
        moonrunes.style.backgroundImage = "url('img/moonrune_icon.png')";
        moonrunes.addEventListener("click", function(){
          var overlay = document.getElementById("overlay");
          var close_overlay = document.createElement("button");
          close_overlay.setAttribute("id", "close-overlay");
          close_overlay.addEventListener("click",function(){
            overlay.style.display = "none";
            overlay.innerHTML = "";
          }, {"once": true});
          overlay.appendChild(close_overlay);
          var moonrunes_page = document.createElement("img");
          moonrunes_page.src = "img/moonrunes.png";
          overlay.appendChild(moonrunes_page);
          overlay.style.display = "inline-block";
          overlay.style.backgroundColor = "white";
          overlay.style.backgroundImage = "url('https://accrete.caltech.edu:8080/img/luna_nova.png')";
          overlay.style.overflow = "scroll";
        });
        document.getElementsByClassName("mapboxgl-ctrl-top-right")[0].children[0].appendChild(moonrunes);
        return [false,{}];
      },
      "suptext":"Announcer Duck",
      "text":"Congratulations, you have unlocked the 'moonrunes' reference sheet!",
      "image":"img/duck.gif"
    },
    {
      "suptext":"Announcer Duck",
      "text":"Congratulations, you have unlocked the 'ghost notebook' totem! You should say the magic words!",
      "image":"img/duck.gif"
    },
    {
      "text": "You haven't worked that hard in years! How do moonrunes even work??"
    },
    {
      "text": "Next on your schedule is magical fitness. Time to take a break!"
    },
    {
      "suptext":"Ursula",
      "image":"img/ursula.png",
      "text": "I know what you're thinking! But magical fitness is more than just movement and coordination."
    },
    {
      "suptext":"Ursula",
      "image":"img/ursula.png",
      "text": "It's about teamwork and friendship and finding your true magical potential!!"
    },
    {
      "suptext":"Ursula",
      "image":"img/ursula.png",
      "text": "Wands at the ready!"
    }
  ],
  "stringpuzzle": [
    {
      "suptext":"Announcer Duck",
      "text":"Congratulations, you have unlocked the 'turtle whistle' totem! Time to say the magic words!",
      "image":"img/duck.gif"
    },
    {
      "text":"Regardless of how you feel about magical training, you can't deny the splendid architecture at Luna Nova Magical Academy."
    },
    {
      "text":"The many spires, buttresses... It certainly is a nice place to study magic."
    },
    {
      "suptext":"Ursula",
      "image":"img/ursula.png",
      "text":"EEEEK! There's an enormous spider on the campus grounds!!"
    },
    {
      "suptext":"Diana",
      "image":"img/diana.png",
      "text":"What was that?? A monster? I'll go check it out!"
    }
  ],
  "tunnels": [
    {
      "suptext":"Announcer Duck",
      "text":"Congratulations, you have unlocked the 'golden heart of monster' totem! Time to say the magic words!",
      "image":"img/duck.gif"
    },
    {
      "text": "The spider might not have proved a challenge for you, but what lies ahead might not be so easy."
    },
    {
      "suptext":"Ursula",
      "image":"img/ursula.png",
      "text":"Ready for my dungeon crawling class? It won't be easy!"
    }
  ],
  "chemistry": [
    {
      "suptext":"Announcer Duck",
      "text":"Congratulations, you have unlocked the 'light bulb' totem! Time to say the magic words!",
      "image":"img/duck.gif"
    },
    {
      "text":"You found yourself last time in the darkness and yet found light in the strength of your bonds!"
    },
    {
      "alternative": function(){
        //play_video("media/lwa-potions.mp4");
        return [false, {}];
      },
      "text":"Next is potions, and Sucy looks to be morbidly delighted!"
    }
  ],
  "rappelling": [
    {
      "alternative": function(){
        //play_video("media/lwa-polaris.mp4");
        return [false, {}];
      },
      "suptext":"Ursula",
      "image":"img/ursula.png",
      "text":"It's time for your flying class!!"
    },
    {
      "suptext":"Announcer Duck",
      "text":"Congratulations, you have unlocked the 'statue of the head of the stag king of the forest' totem! Time to say the magic words!",
      "image":"img/duck.gif"
    },
    {
      "suptext":"Diana",
      "image":"img/diana.png",
      "text":"Pfft, someone like you, who can't even ride a broomstick, flying?"
    },
    {
      "text":"Deep in the ground, or high in the sky, the power of magic is binding and unifying!"
    }
  ],
  "lunch": [
    {
      "alternative": function(){
        //play_video("media/lwa-polaris.mp4");
        return [false, {}];
      },
      "suptext":"Announcer Duck",
      "text":"Congratulations, you have unlocked the 'tarot card with shiny chariot' totem! Time to say the magic words!",
      "image":"img/duck.gif"
    },
    {
      "suptext":"Ursula",
      "image":"img/ursula.png",
      "text":"After all those classes, you must be getting hungry. Why don't you head somewhere to get a bite to eat?"
    }
  ],
  "arboretum": [
    {
      "suptext":"Ursula",
      "image":"img/ursula.png",
      "text":"And now... Let's have a little competition to see whose magical powers are the most adept!"
    },
    {
      "suptext":"Diana",
      "image":"img/diana.png",
      "text":"If you need some help, you know who to ask..! *smirk*"
    },
    {
      "alternative": function(){
        app.app_state.map.flyTo({center:[-118.052481,34.144489]});
        app.team.red.element.style.display = "block";
        app.team.red.element.innerHTML = "0";
        app.team.blue.element.style.display = "block";
        app.team.blue.element.innerHTML = "0";
        var arboretum = document.createElement("button");
        arboretum.setAttribute("id", "moonrunes");
        arboretum.innerHTML = "A"
        arboretum.addEventListener("click", function(){
          var overlay = document.getElementById("overlay");
          overlay.style.zIndex = 6;
          var close_overlay = document.createElement("button");
          close_overlay.setAttribute("id", "close-overlay");
          close_overlay.addEventListener("click",function(){
            overlay.style.display = "none";
            overlay.innerHTML = "";
          }, {"once": true});
          var input_a6 = document.createElement("input");
          input_a6.setAttribute("class", "arboretum");
          input_a6.setAttribute("data-id", "arboretum-6");
          input_a6.setAttribute("data-name", "TROPICAL GREENHOUSE");
          var input_a7 = document.createElement("input");
          input_a7.setAttribute("class", "arboretum");
          input_a7.setAttribute("data-id", "arboretum-7");
          input_a7.setAttribute("data-name", "CANARY ISLANDS PATH");
          var input_a20 = document.createElement("input");
          input_a20.setAttribute("class", "arboretum");
          input_a20.setAttribute("data-id", "arboretum-20");
          input_a20.setAttribute("data-name", "DAYLILY COLLECTION");
          var input_a21 = document.createElement("input");
          input_a21.setAttribute("class", "arboretum");
          input_a21.setAttribute("data-id", "arboretum-21");
          input_a21.setAttribute("data-name", "HERB GARDEN");
          var input_a26 = document.createElement("input");
          input_a26.setAttribute("class", "arboretum");
          input_a26.setAttribute("data-id", "arboretum-26");
          input_a26.setAttribute("data-name", "COACH BARN");
          var input_a27 = document.createElement("input");
          input_a27.setAttribute("class", "arboretum");
          input_a27.setAttribute("data-id", "arboretum-27");
          input_a27.setAttribute("data-name", "QUEEN ANNE COTTAGE");
          var input_a31 = document.createElement("input");
          input_a31.setAttribute("class", "arboretum");
          input_a31.setAttribute("data-id", "arboretum-31");
          input_a31.setAttribute("data-name", "PREHISTORIC FOREST");
          var input_baldwin = document.createElement("input");
          input_baldwin.setAttribute("class", "arboretum");
          input_baldwin.setAttribute("data-id", "baldwin");
          input_baldwin.setAttribute("data-name", "BALDWIN LAKE");
          input_a6.setAttribute("id", "arboretum-6");
          input_a7.setAttribute("id", "arboretum-7");
          input_a20.setAttribute("id", "arboretum-20");
          input_a21.setAttribute("id", "arboretum-21");
          input_a26.setAttribute("id", "arboretum-26");
          input_a27.setAttribute("id", "arboretum-27");
          input_a31.setAttribute("id", "arboretum-31");
          input_baldwin.setAttribute("id", "baldwin");
          /*var input_norton = document.createElement("input");
          input_a6.setAttribute("class", "arboretum");*/
          var cool = document.createElement("div");
          cool.style.padding = "16px";
          cool.style.fontSize = "26px";
          cool.innerHTML = "Write your answers separated by commas! Your work will be saved if you close this window";
          overlay.appendChild(cool);
          list_inputs = [input_a6,input_a7,input_a20,input_a21,input_a26,input_a27,input_a31,input_baldwin];
          for(var oidx=0; oidx < list_inputs.length; oidx++){
            var cool = document.createElement("div");
            cool.setAttribute("class", "suptext");
            cool.style.padding = "16px";
            cool.innerHTML = list_inputs[oidx].getAttribute("data-name");
            overlay.appendChild(cool);
            overlay.appendChild(list_inputs[oidx]);
          }
          socket.emit("need_points");
          for(var aridx=0; aridx<document.getElementsByClassName("arboretum").length; aridx++){
            document.getElementsByClassName("arboretum")[aridx].addEventListener("keydown", function(e){
              var code = e.keyCode ? e.keyCode : e.which;
              if (code === 13) {
                socket.emit(this.getAttribute("data-id"), {"team": Cookies.get("team"), "answer": this.value});
                this.blur();
              }
            });
          }
          overlay.appendChild(close_overlay);
          overlay.style.display = "inline-block";
          overlay.style.backgroundColor = "white";
          overlay.style.backgroundImage = "url('https://accrete.caltech.edu:8080/img/luna_nova.png')";
          overlay.style.overflow = "scroll";
        });
        document.getElementsByClassName("mapboxgl-ctrl-top-right")[0].children[0].appendChild(arboretum);
        return [false, {}];
      },
      "text": "The scene is set for a magical showdown! Akko, Sucy, and Lotte against Constanze, Jasminka, and Amanda!"
    }
  ],
  "norton": [
    {
      "alternative": function(){
        app.app_state.map.flyTo({center:[-118.159257,34.146347]});
        app.team.red.element.style.display = "block";
        app.team.blue.element.style.display = "block";
        var norton = document.createElement("button");
        norton.setAttribute("id", "moonrunes");
        norton.innerHTML = "N"
        norton.addEventListener("click", function(){
          var overlay = document.getElementById("overlay");
          overlay.style.zIndex = 6;
          var close_overlay = document.createElement("button");
          close_overlay.setAttribute("id", "close-overlay");
          close_overlay.addEventListener("click",function(){
            overlay.style.display = "none";
            overlay.innerHTML = "";
          }, {"once": true});
          var input_norton = document.createElement("input");
          input_norton.setAttribute("class", "arboretum");
          input_norton.setAttribute("data-id", "norton");
          input_norton.setAttribute("id", "norton");
          input_norton.setAttribute("data-name", "What is the sentence?");
          /*var input_norton = document.createElement("input");
          input_a6.setAttribute("class", "arboretum");*/
          var cool = document.createElement("div");
          cool.setAttribute("class", "suptext");
          cool.style.padding = "20px";
          cool.innerHTML = input_norton.getAttribute("data-name");
          overlay.appendChild(cool);
          overlay.appendChild(input_norton);
          overlay.appendChild(close_overlay);
          socket.emit("need_points");
          input_norton.addEventListener("keydown", function(e){
            var code = e.keyCode ? e.keyCode : e.which;
              if (code === 13) {
                socket.emit("norton", {"team": Cookies.get("team"), "answer": input_norton.value});
              }
            });
          overlay.style.display = "inline-block";
          overlay.style.backgroundColor = "white";
          overlay.style.backgroundImage = "url('https://accrete.caltech.edu:8080/img/luna_nova.png')";
          overlay.style.overflow = "scroll";
        });
        document.getElementsByClassName("mapboxgl-ctrl-top-right")[0].children[0].appendChild(norton);
        return [false, {}];
      },
      "text": "The scene is set for round two!!! Akko, Sucy, and Lotte against Constanze, Jasminka, and Amanda!"
    },
    {
      "suptext":"Ursula",
      "image":"img/ursula.png",
      "text":"Ooh! I wonder who will win!!!"
    },
    {
      "suptext":"Diana",
      "image":"img/diana.png",
      "text":"Oh please. It's so obvious!"
    },
    {
      "suptext":"Announcer Duck",
      "text":"Points are doubled for this round!! Good luck!!!",
      "image":"img/duck.gif"
    }
  ],
  "desert": [
    {
      "text":"Believing is your magic! <3"
    }
  ]
}
