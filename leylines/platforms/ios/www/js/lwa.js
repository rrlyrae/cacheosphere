function start_geoloc(){
  /*var output = document.getElementById("location");*/
  if ("geolocation" in navigator) {
    /* geolocation is available */
    function geolocation_success(position){
      var latitude = position.coords.latitude;
      var longitude = position.coords.longitude;
      /*var output = document.getElementById("location");
      output.innerHTML = "Lat "+latitude+", Long "+longitude+".";*/
      app.app_state["latitude"] = latitude;
      app.app_state["longitude"] = longitude;
      app.ui.update_text("lat"+latitude+"long"+longitude);
    }
    function geolocation_error(error) {
      alert("Error in HTML5 Geolocation. Code: "+ error.code + " Message: " + error.message);
    }
    navigator.geolocation.getCurrentPosition(geolocation_success, geolocation_error, {enableHighAccuracy: true});
    navigator.geolocation.watchPosition(geolocation_success, geolocation_error, {enableHighAccuracy: true});
    init_map();
  } else {
    /* geolocation IS NOT available */
    alert("HTML5 Geolocation not available.");
  }

  /*loadJSON('map-data.json',
  init_map,
  function(xhr){alert("Error loading JSON. "+xhr)});*/
}

function loadJSON(path, success, error){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function(){
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        if (success) success(JSON.parse(xhr.responseText));
      } else {
        if (error) error(xhr);
      }
    }
  };
  xhr.open("GET", path, true);
  xhr.send();
}

function load_geojson(data){
  //console.log(data);
  //var dataLayer = L.geoJson(data);
  //dataLayer.addTo(app.app_state["map"])
  opts = {
    "id": "points",
    "type": "symbol",
    "source": {
      "type": "geojson",
      "data": data
    },
    "layout": {
        "icon-image": "{icon}-15",
        "text-field": "{title}",
        "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
        "text-offset": [0, 0.6],
        "text-anchor": "top"
    }
  }
  app.app_state["map"].on("load",function(){
    app.app_state["map"].addLayer(opts);
    console.log('map loaded')
  });
}

function init_map(){
  /*app.app_state["map"] = L.map('map',{attributionControl:false}).setView([34.1402975,-118.1339264], 14);
  var terrainTiles = L.tileLayer.provider('OpenTopoMap');
  terrainTiles.addTo(app.app_state["map"]);
  var style = {
    "clickable": true,
    "color": "#00D",
    "fillColor": "#00D",
    "weight": 1.0,
    "opacity": 0.3,
    "fillOpacity": 0.2
  };
  var hover_style = {
      "fillOpacity": 0.5
  };
  var options = {
    "clipTiles": true,
    "unique": function(feature){
      return feature.id;
    }
  }
  var geojson_options = {
    "style": style,
    "onEachFeature": function(feature, layer){
      if(feature.properties){
        var popup_string = '<div class="popup">';
        for (var k in feature.properties){
          var v = feature.properties[k];
          popup_string = k + ': ' + v + '<br />';
          console.log(popup_string)
        }
        popup_string += '</div>';
        layer.bindPopup(popup_string);
      }
      if(!(layer instanceof L.Point)){
        layer.on("mouseover", function(){
          layer.setStyle(hover_style);
        });
        layer.on("mouseout", function(){
          layer.setStyle(style);
        });
      }
    }
  }*/
  var loc = location.href;
  if(app.web){
    var geojson_url = loc+"js/map-data.json";
  }
  else var geojson_url = loc.substring(0,loc.length-11)+'/js/map-data.json';
  loadJSON(geojson_url, load_geojson, function(error){
    alert("Error in obtaining GeoJSON resources: " + error);
  });
  //document.getElementById("map").setAttribute("style","width:"+window.innerWidth+"px;height:"+window.innerHeight+"px");
  mapboxgl.accessToken = 'pk.eyJ1IjoiaXJseXJhZSIsImEiOiJjajJoNzh5bnowMDduMzJuNWd5bHV3a2FlIn0.uEz7bdUVlY1UBtuXIfrweA';
  app.app_state["map"] = new mapboxgl.Map({
      container: 'map', // container id
      style: 'mapbox://styles/mapbox/streets-v9',
      center: [-118.1339264,34.1402975], // starting position
      zoom: 14, // starting zoom,
      attributionControl:false
  });

  // Add zoom and rotation controls to the map.
  app.app_state["map"].addControl(new mapboxgl.NavigationControl(), 'top-left');
}

function init_ui(){
  app["ui"] = {
    "element": document.getElementById("ui"),
    "speed": 1,
    "text_queue": [],
    "update_text": function(text){
      app.ui.text_queue.push(text);
      if(typeof app.ui.element.innerHTML !== 'undefined'){
        if(app.ui.element.innerHTML.length < 1){
          app.ui.next_text();
        }
      }
      else {
        console.log(app.ui.element.innerHTML)
        app.ui.next_text();
      }
    },
    "update_text_block": function(text_arr){
      for(var idx=0;idx<text_arr.length;idx++){app.ui.text_queue.push(text_arr[idx])}
      if(typeof app.ui.element.innerHTML !== 'undefined'){
        if(app.ui.element.innerHTML.length < 1){
          app.ui.next_text();
        }
      }
      else {
        app.ui.next_text();
      }
    },
    "next_text": function(){
      width = window.innerWidth
      if (app.ui.text_queue.length > 0){
        app.ui.element.innerHTML = app.ui.text_queue[0]
        app.ui.text_queue.shift()
        app.ui.element.style = "animation:fadein 2s;display:block;width:"+width+"px;bottom: 0px;left: 50%;transform: translate(-50%,0%);"
      }
      else {
        app.ui.element.innerHTML = ""
        app.ui.element.style = "display:none;width:"+width+"px;bottom: 0px;left: 50%;transform: translate(-50%,0%);"
      }
    }
  }
  app.ui.element.onclick = app.ui.next_text
  text_test = ["hello world!", "this is a test!", "this is the end of the test for ui."]
  app.ui.update_text_block(text_test)
}

//window.onorientationchange = function
