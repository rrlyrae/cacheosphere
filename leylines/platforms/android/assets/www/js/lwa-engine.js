/* Storymode engine, progression of RPG elements...
   Feeds in a collection of js files corresponding to the story plotline.
   The plot engine simply has the character information (for populating the UI)
   and the current queue of actions (read in from plot/*.js). */

var character = {
  "location": [0.,0.],
  "inventory": [],
  "progress": []
}
