var fs = require('fs'),
http = require('http'),
https = require('https'),
express = require('express'),
nocache = require('nocache');

var port = 8080;

var options = {
    key: fs.readFileSync('/home/irlyrae/accrete.key'),
    cert: fs.readFileSync('/home/irlyrae/accrete.pem'),
};

var app = express();

app.use(nocache());
app.use(express.static("../leylines/www/"));

var server = https.createServer(options, app);

var io = require('socket.io')(server);
//start
var global_data = {
  "users": [],
  "teams":{
    "red":{"points": 0., "answers": {}, "point_distr": {}},
    "blue":{"points": 0., "answers": {}, "point_distr": {}}
  },
  "state": "lunch",
  "state_num": 7
}

function compare(a,b) {
  if (a.timestamp < b.timestamp)
    return -1;
  if (a.timestamp > b.timestamp)
    return 1;
  return 0;
}

var contents = fs.readFileSync("adam_amy_trace.json");
var trace = JSON.parse(contents);
trace.sort(compare);
var quest_order = ["breakfast", "moonrunes", "rhythm", "stringpuzzle", "tunnels", "chemistry", "rappelling", "lunch", "arboretum", "norton", "desert"];
var location = {
  "breakfast": [-118.122142,34.136848],
  "moonrunes": [-118.1266742,34.137223],
  "rhythm": [-118.1254692,34.13968],
  "stringpuzzle": [-118.1250262,34.136957],
  "tunnels": [-118.125674,34.138560],
  "chemistry": [-118.1271612,34.1375542],
  "rappelling": [-118.123832,34.136701],
  "lunch": [-118.120885,34.139061],
  "arboretum": [-118.052481,34.144489],
  "norton": [-118.159257,34.146347]
}

var trace_dict = {}
for (var idx = 0; idx < quest_order.length; idx++){
  trace_dict[quest_order[idx]] = {"timestamps": [], "coords": []};
}

for (var idx = 0; idx < trace.length; idx++){
  trace_dict[quest_order[trace[idx]["rec_idx"]]]["timestamps"].push(trace[idx]["timestamp"]);
  trace_dict[quest_order[trace[idx]["rec_idx"]]]["coords"].push([trace[idx]["longitude"], trace[idx]["latitude"]]);
}
for (var idx = 0; idx < quest_order.length; idx++){
  var timestamp_first = trace_dict[quest_order[idx]]["timestamps"][0];
  for (var trace_idx = 0; trace_idx < trace_dict[quest_order[idx]]["timestamps"].length; trace_idx++){
    trace_dict[quest_order[idx]]["timestamps"][trace_idx] -= timestamp_first;
  }
}

var events = require('events');
var eventEmitter = new events.EventEmitter();
function add_diana(){
  global_data.users.push({"name": "Diana",
  "team": "red",
  "sprite": "diana",
  "coords": [0.,0.]})
}
add_diana();

function speedup(factor, timestamps){
  for (var idx=0; idx < timestamps.length; idx++){
    timestamps[idx] /= factor;
  }
  return timestamps;
}

function update_diana_loc (timestamp, speedup_factor){
  var curr_coords = trace_dict[global_data.state]["coords"];
  var curr_timestamps = speedup(speedup_factor, trace_dict[global_data.state]["timestamps"]);
  return function(){
    console.log(+ new Date(), curr_timestamps.length)
    for (var idx = 0; idx < curr_timestamps.length; idx++){
      console.log(idx);
      new_ts = + new Date();
      user_list = [];
      for (var nidx = 0; nidx < global_data.users.length; nidx++) {
        user_list.push(global_data.users[nidx].name);
      }
      if(curr_timestamps[idx] > new_ts - timestamp){
        global_data.users[user_list.indexOf("Diana")]["coords"] = curr_coords[idx];
        console.log(global_data.users[user_list.indexOf("Diana")]["coords"]);
        curr_coords = curr_coords.slice(idx);
        curr_timestamps = curr_timestamps.slice(idx);
        return true;
      }
    }
    eventEmitter.emit("del_trigger");
  }
}
function run_diana_plot(speedup_factor){
  var timeout_id = setInterval(update_diana_loc(+ new Date(), speedup_factor),1000);
  eventEmitter.on("del_trigger", function(){
    clearInterval(timeout_id);
  });
}

var teams = ["red", "blue"];
var team_join_order = ["red", "red", "red", "red", "blue", "blue", "blue"];
var sprites = ["diana", "akko", "lotte", "sucy", "constanze", "amanda", "jasminka"];
var doing_gps_broadcast = false;
var gps_trace = [];

function socket_code (socket) {
  (function() {
    var emit = socket.emit;
    socket.emit = function() {
      console.log('***','emit', Array.prototype.slice.call(arguments));
      emit.apply(socket, arguments);
    };
    var $emit = socket.$emit;
    socket.$emit = function() {
      console.log('***','on',Array.prototype.slice.call(arguments));
      $emit.apply(socket, arguments);
    };
  })();
  socket.on('name', function(data){
    function do_gps_broadcast(){
      io.sockets.emit('gps', global_data.users);
    console.log("gps", global_data.users)}
    console.log("name",data);
    var user_list = [];
    for (var idx = 0; idx < global_data.users.length; idx++){
      user_list.push(global_data.users[idx].name);
    }
    if(user_list.indexOf(data)>-1e-5){
      //they're in the array
      try {
        socket.emit('notice', {
          "suptext":"Ursula",
          "text":"Welcome back to Luna Nova Academy, "+data+"!",
          "image":"img/ursula.png"
        });
      }
      catch (e) {
        console.log(e);
      }
    }
    else {
      global_data.users.push({"name": data,
      "team": team_join_order[global_data.users.length],
      "sprite": sprites[global_data.users.length],
      "coords": [0.,0.]});
      if (global_data.users.length > 6){
        make_diana_run(3);
      }
      try {
        socket.emit('notice', {
          "suptext":"Ursula",
          "text":"Thank you, "+data+"! Welcome to Luna Nova Academy!",
          "image":"img/ursula.png",
          "refresh": true
        });
      }
      catch (e) {
        console.log(e);
      }
    }
    var user_list = [];
    for (var idx = 0; idx < global_data.users.length; idx++){
      user_list.push(global_data.users[idx].name);
    }
    try {
      socket.emit('name_success', {
        "name": data,
        "team": global_data.users[user_list.indexOf(data)]["team"],
        "sprite": global_data.users[user_list.indexOf(data)]["sprite"],
        "coords": global_data.users[user_list.indexOf(data)]["coords"]
      });
    }
    catch (e){
      console.log(e);
    }
    if(!doing_gps_broadcast){
      setInterval(do_gps_broadcast, 1000);
      doing_gps_broadcast = true;
    }
  });
  socket.on('gps_update', function(data){
    var user_list = [];
    for (var idx = 0; idx < global_data.users.length; idx++) {
      user_list.push(global_data.users[idx].name);
    }
    try {
      global_data.users[user_list.indexOf(data.name)]["coords"] = data["coords"];
    }
    catch (e){
      console.log(e);
    }
  })

  function get_diana_message(){
    msgs = ["Follow me!",
    "Keep up, will you!",
    "You seem to be wandering rather aimlessly...",
    "Let me show you where to go."]
    return msgs[Math.floor(Math.random()*msgs.length)]
  }
  function make_diana_run(data){
    if (typeof data != "number"){
      data = 1.
    }
    io.sockets.emit('notice', {
      "suptext":"Diana",
      "text": get_diana_message(),
      "image":"img/diana.png"
    });
    eventEmitter.on("del_trigger", function(){
      //io.sockets.emit("hide_loc_marker");
    });
    run_diana_plot(data);
    //io.sockets.emit("move_loc_marker", location[global_data.state])
  }
  socket.on('advance_state', function(data){
    if (global_data.state_num < 10){
      global_data.state_num += 1;
      global_data.state = quest_order[global_data.state_num];
      io.sockets.emit("state", global_data.state_num);
      if (global_data.state_num < 8){
        make_diana_run(1.5);
      }
    }
    else {
      console.log("tried to advance state", global_data.state_num)
    }
  });
  socket.on('need_state', function(data){
    try {
      socket.emit('state', global_data.state_num);
    }
    catch (e) {
      console.log(e);
    }
  })
  socket.on('diana_run', make_diana_run);
  socket.on('disconnect', function(){});
  socket.on('admin_msg', function(data){
    try {
      io.sockets.emit('notice', {
        "suptext":"Ursula",
        "text": data,
        "image":"img/ursula.png"
      });
    }
    catch (e) {
      console.log(e);
    }
  });
  socket.on('admin_record', function(data){
    gps_trace.push(data);
    console.log(gps_trace.length);
  });
  socket.on('done_admin_record', function(data){
    fs.writeFile('trace.json',JSON.stringify(gps_trace),function(err){
      if(err) throw err;
    });
  });
  function one_point_from_tf(tf){
    if(tf){
      return 1.;
    }
    else {
      return 0.;
    }
  }
  function do_sum( obj ) {
    var sum = 0;
    for( var el in obj ) {
      if( obj.hasOwnProperty( el ) ) {
        sum += obj[el];
      }
    }
    return sum;
  }
  function recalc_points(){
    global_data.teams.red.points = do_sum(global_data.teams.red.point_distr);
    global_data.teams.blue.points = do_sum(global_data.teams.blue.point_distr);
  }
  if(typeof(String.prototype.trim) === "undefined")
  {
      String.prototype.trim = function()
      {
          return String(this).replace(/^\s+|\s+$/g, '');
      };
  }
  socket.on('need_points', function(){
    io.sockets.emit("points", global_data.teams)
  });
  socket.on('arboretum-6', function(data){
    var team = data.team;
    var given_answer = data.answer.toLowerCase();
    var correct_answers = ["guajacum officinale lignum-vitae trop amer", "microcycas calocoma", "aristolochia labiata"];
    var points_given = 0.;
    var answers = given_answer.split(",");
    answers = answers.filter (function (value, index, array) {
      return array.indexOf (value) == index;
    });
    for (var idx = 0; idx < answers.length; idx++){
      points_given += one_point_from_tf(correct_answers.indexOf(answers[idx].trim()) > -1);
    }
    global_data.teams[team].answers['arboretum-6'] = given_answer;
    global_data.teams[team].point_distr['arboretum-6'] = points_given;
    recalc_points();
    io.sockets.emit("points", global_data.teams);
    socket.emit("notice", {
      "suptext":"Announcer Duck",
      "text":"Your answer for Tropical Greenhouse earned "+points_given+" points.",
      "image":"img/duck.gif"
    });
  });
  socket.on('arboretum-7', function(data){
    var team = data.team;
    var given_answer = data.answer.toLowerCase();
    var correct_answers = ["dracaena draco"];
    var points_given = 0.;
    var answers = given_answer.split(",");
    answers = answers.filter (function (value, index, array) {
      return array.indexOf (value) == index;
    });
    for (var idx = 0; idx < answers.length; idx++){
      points_given += one_point_from_tf(correct_answers.indexOf(answers[idx].trim()) > -1);
    }
    global_data.teams[team].answers['arboretum-7'] = given_answer;
    global_data.teams[team].point_distr['arboretum-7'] = points_given;
    recalc_points();
    io.sockets.emit("points", global_data.teams);
    socket.emit("notice", {
      "suptext":"Announcer Duck",
      "text":"Your answer for Canary Islands earned "+points_given+" points.",
      "image":"img/duck.gif"
    });
  });
  socket.on('arboretum-20', function(data){
    var team = data.team;
    var given_answer = data.answer.toLowerCase();
    var correct_answers = [3994,-6,5978,15818,2003,5997,14,2,10,3958000];
    var points_given = 0.;
    var answers = given_answer.split(",");
    answers = answers.filter (function (value, index, array) {
      return array.indexOf (value) == index;
    });
    for (var idx = 0; idx < answers.length; idx++){
      for (var nidx = 0; nidx < correct_answers.length; nidx++){
        if(Math.abs(Number(answers[idx]) - correct_answers[nidx]) < 0.1){
          points_given += 1;
        }
      }
    }
    global_data.teams[team].answers['arboretum-20'] = given_answer;
    global_data.teams[team].point_distr['arboretum-20'] = points_given;
    recalc_points();
    io.sockets.emit("points", global_data.teams);
    socket.emit("notice", {
      "suptext":"Announcer Duck",
      "text":"Your answer for Daylily Collection earned "+points_given+" points.",
      "image":"img/duck.gif"
    });
  });
  socket.on('arboretum-21', function(data){
    var team = data.team;
    var given_answer = data.answer.toLowerCase();
    var correct_answers = ["brugmansia versicolor", "monarda didyma", "sisyrinchium bellum", "buddleja davidii", "asclepias curassavica", "teucrium marium", "prunella vulgaris", "catha edulis", "duranta erecta"];
    var points_given = 0.;
    var answers = given_answer.split(",");
    answers = answers.filter (function (value, index, array) {
      return array.indexOf (value) == index;
    });
    for (var idx = 0; idx < answers.length; idx++){
      points_given += one_point_from_tf(correct_answers.indexOf(answers[idx].trim()) > -1);
    }
    global_data.teams[team].answers['arboretum-21'] = given_answer;
    global_data.teams[team].point_distr['arboretum-21'] = points_given;
    recalc_points();
    io.sockets.emit("points", global_data.teams);
    socket.emit("notice", {
      "suptext":"Announcer Duck",
      "text":"Your answer for Herb Garden earned "+points_given+" points.",
      "image":"img/duck.gif"
    });
  });
  socket.on('arboretum-26', function(data){
    var team = data.team;
    var given_answer = data.answer.toLowerCase();
    var correct_answers = ["laredo", "silver cloud", "lillita", "california", "estrellita", "el picato", "rey el tovar", "lady diamond"];
    var points_given = 0.;
    var answers = given_answer.split(",");
    answers = answers.filter (function (value, index, array) {
      return array.indexOf (value) == index;
    });
    for (var idx = 0; idx < answers.length; idx++){
      points_given += one_point_from_tf(correct_answers.indexOf(answers[idx].trim()) > -1);
    }
    global_data.teams[team].answers['arboretum-26'] = given_answer;
    global_data.teams[team].point_distr['arboretum-26'] = points_given;
    recalc_points();
    io.sockets.emit("points", global_data.teams);
    socket.emit("notice", {
      "suptext":"Announcer Duck",
      "text":"Your answer for Coach Barn earned "+points_given+" points.",
      "image":"img/duck.gif"
    });
  });
  socket.on('arboretum-27', function(data){
    var team = data.team;
    var given_answer = data.answer.toLowerCase();
    var correct_answers = ["16", "17", "hotel oakwood", "harp","tweed","venus"];
    var points_given = 0.;
    var answers = given_answer.split(",");
    answers = answers.filter (function (value, index, array) {
      return array.indexOf (value) == index;
    });
    for (var idx = 0; idx < answers.length; idx++){
      points_given += one_point_from_tf(correct_answers.indexOf(answers[idx].trim()) > -1);
    }
    global_data.teams[team].answers['arboretum-27'] = given_answer;
    global_data.teams[team].point_distr['arboretum-27'] = points_given;
    recalc_points();
    io.sockets.emit("points", global_data.teams);
    socket.emit("notice", {
      "suptext":"Announcer Duck",
      "text":"Your answer for Queen Anne Cottage earned "+points_given+" points.",
      "image":"img/duck.gif"
    });
  });
  socket.on('arboretum-31', function(data){
    var team = data.team;
    var given_answer = data.answer.toLowerCase();
    var correct_answers = ["caryota gigas", "nolina recurvata", "metasequoia glyptostroboides", "sprankle"];
    var points_given = 0.;
    var answers = given_answer.split(",");
    answers = answers.filter (function (value, index, array) {
      return array.indexOf (value) == index;
    });
    for (var idx = 0; idx < answers.length; idx++){
      points_given += one_point_from_tf(correct_answers.indexOf(answers[idx].trim()) > -1);
    }
    global_data.teams[team].answers['arboretum-31'] = given_answer;
    global_data.teams[team].point_distr['arboretum-31'] = points_given;
    recalc_points();
    io.sockets.emit("points", global_data.teams);
    socket.emit("notice", {
      "suptext":"Announcer Duck",
      "text":"Your answer for Prehistoric Forest earned "+points_given+" points.",
      "image":"img/duck.gif"
    });
  });
  socket.on('baldwin', function(data){
    var team = data.team;
    var given_answer = data.answer.toLowerCase();
    var correct_answers = ["8"];
    var points_given = 0.;
    var answers = given_answer.split(",");
    answers = answers.filter (function (value, index, array) {
      return array.indexOf (value) == index;
    });
    for (var idx = 0; idx < answers.length; idx++){
      points_given += one_point_from_tf(correct_answers.indexOf(answers[idx].trim()) > -1);
    }
    global_data.teams[team].answers['baldwin'] = given_answer;
    global_data.teams[team].point_distr['baldwin'] = points_given;
    recalc_points();
    io.sockets.emit("points", global_data.teams);
    socket.emit("notice", {
      "suptext":"Announcer Duck",
      "text":"Your answer for Baldwin Lake earned "+points_given+" points.",
      "image":"img/duck.gif"
    });
  });
  socket.on('norton', function(data){
    var team = data.team;
    var given_answer = data.answer.toLowerCase();
    var correct_answers = ["seeds","draw","faith","at", "water", "so", "the", "flame", "stayed", "reserved"];
    var points_given = 0.;
    var answers = given_answer.split(" ");
    answers = answers.filter (function (value, index, array) {
      return array.indexOf (value) == index;
    });
    for (var idx = 0; idx < answers.length; idx++){
      points_given += 2.*one_point_from_tf(correct_answers.indexOf(answers[idx].trim()) > -1);
    }
    global_data.teams[team].answers['norton'] = given_answer;
    global_data.teams[team].point_distr['norton'] = points_given;
    recalc_points();
    io.sockets.emit("points", global_data.teams);
    socket.emit("notice", {
      "suptext":"Announcer Duck",
      "text":"Your answer for Norton-Simon earned "+points_given+" points.",
      "image":"img/duck.gif"
    });
  });
}

io.on('connection', socket_code);
var readline = require('readline');
var eval = require('eval');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
server.listen(port, "0.0.0.0", function(){
  function do_run(code){
    try {
      eval(code,{"app": app, "io": io, "global_data": global_data},true);//"eval('"+code+"')"
    }
    catch (e) {
      console.log(e);
    }
    finally {
      rl.question(">>> ", do_run)
    }
  }
  console.log("Express server listening on port " + port);
  rl.question(">>> ", do_run);
});
